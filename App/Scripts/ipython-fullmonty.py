#!C:\FullMonty275\App\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'ipython','console_scripts','ipython'
__requires__ = 'ipython'
import sys
from pkg_resources import load_entry_point


if __name__ == '__main__':
	
	import os
	if sys.platform.lower().startswith( 'win' ): home = os.environ[ 'USERPROFILE' ]
	else: home = os.environ[ 'HOME' ]
	startup_dir = os.path.join( home, '.ipython', 'profile_default', 'startup' )
	startup_file = os.path.join( startup_dir, 'FullMontyShortcuts.ipy' )
	if not os.path.isdir( startup_dir ):
		os.makedirs( startup_dir )
		open( startup_file, 'wt' ).write("try: import BCPy2000.Shortcuts\nexcept ImportError: print 'Failed to import BCPy2000.Shortcuts'\n")
	
	sys.exit(
		load_entry_point('ipython', 'console_scripts', 'ipython')()
	)
