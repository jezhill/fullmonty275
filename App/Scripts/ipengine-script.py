#!C:\Users\Jeremy\Desktop\FullMonty275\App\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'ipython==0.13.2','console_scripts','ipengine'
__requires__ = 'ipython==0.13.2'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('ipython==0.13.2', 'console_scripts', 'ipengine')()
    )
