:: With Python, we inevitably tend to end up with multiple distributions.
:: 
:: In order for this to work:
:: (1)  Your application may need to know where Python is.
:: (2)  Python will need to know where its own files are.
:: 
:: For this reason, your application may need to call this file,
:: SelectPython.bat
:: 
:: The location of Python (the PYTHONHOME variable, which on Windows
:: should be the parent directory of python.exe) can be set in the
:: following ways (in this order of precedence):
:: 
:: 1. explicitly according to the first command-line argument to this script
::    (a directory, or a quoted semicolon-delimited list of directories - 
::    note that quotes are crucial if there are semicolons in the string)
:: 
:: 2. the directory in which this script resides, if a python.exe also
::    exists there (can be denoted explicitly by - )
:: 
:: 3. the working directory (as usual, can be denoted explicitly by . )
:: 
:: This script determines the full path to Python's home, and set the
:: "PYTHONHOME" environment variable to that. It also adds the PYTHONHOME
:: directory, delimited by a semicolon, to the list of paths in the
:: environment variable PYTHONPATH, along with its subdirectory
:: %PYTHONHOME%\Lib\site-packages .  Together, these measures solve
:: problem (2). We also add %PYTHONHOME% (and its Scripts subdir) to the
:: beginning of the system environment variable "Path":  that
:: solves problem (1).
:: 
:: You can of course manipulate these environment variables permanently
:: ( Control Panel-> System -> Advanced -> Environment Variables )
:: if you wish to permanently "install" one particular Python distro
:: in a fixed location, but then you have to edit them manually again
:: whenever you want to switch.
:: 
:: You can also proceed to call Python "inline" via this command - e.g.:
::
::    SelectPython C:\Python27 python -c "import sys; print(sys.version)"
:: 
:: (A more thorough test is provided in the accompanying script
:: TestSelectedPython.bat)
:: 
:: Note that if you call Python inline, the PYTHONHOME, PYTHONPATH and PATH
:: variables are not exported - they are set only for the child process.
:: Otherwise they are exported for the rest of your session.

@echo off

:: The last-resort default will be the current working directory (of course,
:: you can pass a . to make this explicit).
set "SELECTEDPYTHON=%CD%

:: If this script resides in a Python distro (right next to python.exe)
:: then its parent directory becomes the default (pass a single dash, -,
:: as the argument to explicitly denote this).
if exist "%~dp0\python.exe" set "SELECTEDPYTHON=%~dp0
if             "%~1" == "-" set "SELECTEDPYTHON=%~dp0

:: ...but if a directory (or semicolon-delimited list of directories, in
:: quotes) is given explicitly on the command-line, then use that.
if "%~1" == ""  goto :SkipSearch
if "%~1" == "-" goto :SkipSearch
:: Note: if the caller calls this script with multiple semicolon-delimited
:: possibilities but DOESN'T surround the whole thing with quotes, the shell
:: will actually break that string at the semicolons and distribute it
:: between %1, %2, etc (as if the semicolons were spaces). So, if you have a
:: semicolon, use quotes:
::     right:   SelectPython "%PATH%" ...  
::     wrong:   SelectPython %PATH% ...
set "CANDIDATES=%~1
for %%A in ("%CANDIDATES:;=";"%") do (
	if not "%%~A" == "" set SELECTEDPYTHON="%%~A"
	if not "%%~A" == "" if exist "%%~A\python.exe" goto :SkipSearch
)
:SkipSearch

if not exist "%SELECTEDPYTHON%" (
	echo could not find directory %SELECTEDPYTHON%
	exit /b 1
)
if not exist "%SELECTEDPYTHON%\python.exe" (
	echo could not find python.exe in %SELECTEDPYTHON%
	exit /b 2
)

:: Canonicalize the resulting path:
pushd %SELECTEDPYTHON%
set "SELECTEDPYTHON=%CD%
popd

:: If there's anything to execute inline, make the environment changes transient only (don't export)
if not @%2 == @ setlocal

:: Let Python know where its own files are:
set "PYTHONHOME=%SELECTEDPYTHON%
set "PYTHONPATH=%PYTHONHOME%;%PYTHONHOME%\Lib\site-packages
:: Put Python's location at the beginning of the system path if it's not there already:
echo %PATH% | findstr /i /b /c:"%PYTHONHOME%;" > nul || set "PATH=%PYTHONHOME%;%PYTHONHOME%\Scripts;%PATH%

:: Now execute the rest of the arguments inline, if any:
:: (adapted from http://stackoverflow.com/a/34005255/3019689 )
shift
if @%1 == @ goto :eof
set command=
:BuildCommand
if @%1 == @ goto :CommandFinished
set "command=%command% %1"
shift
goto :BuildCommand
:CommandFinished
:: Chain execution to the specified command
%command%
